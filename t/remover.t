#!/usr/bin/perl -w
# Copyright (C) 2013-2016 all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
use strict;
use warnings;
use Test::More;
use Ssoma::MDA;
use Ssoma::Git;
use Ssoma::Remover;
use Email::MIME;
use Digest::SHA qw/sha1_hex/;
use File::Temp qw/tempdir/;

my $tmpdir = tempdir('ssoma-remover-XXXXXX', TMPDIR => 1, CLEANUP => 1);
my $git_dir = "$tmpdir/gittest";
my $git = Ssoma::Git->new($git_dir);
$git->init_db('-q');
my $mda = Ssoma::MDA->new($git);
my $rm = Ssoma::Remover->new($git);
my @tree;

{
	my $email = Email::MIME->new(<<'EOF');
From: me@example.com
To: u@example.com
Message-ID: <666@example.com>
Subject: zzz

OMFG
EOF

	$mda->deliver($email);
	@tree = `GIT_DIR=$git_dir git ls-tree -r HEAD`;
	is($?, 0, "no error from git ls-tree");
	is(scalar @tree, 1, "message delivered");

	# mime removal
	$rm->remove_mime($email);
	@tree = `GIT_DIR=$git_dir git ls-tree -r HEAD`;
	is($?, 0, "no error from git ls-tree");
	is(scalar @tree, 0, "tree is now empty after removal");

	$mda->deliver($email);
	$email->body_set("conflict");
	$mda->deliver($email);

	@tree = `GIT_DIR=$git_dir git ls-tree -r HEAD`;
	is($?, 0, "no error from git ls-tree");
	is(scalar @tree, 2, "both messages stored");

	# remove only one (the concflicting one)
	$rm->remove_mime($email);
	@tree = `GIT_DIR=$git_dir git ls-tree -r HEAD`;
	is($?, 0, "no error from git ls-tree");
	is(scalar @tree, 1, "one removed, one exists");

	my @line = split(/\s+/, $tree[0]);
	is($line[1], "blob", "back to one blob");
	my $cur = `GIT_DIR=$git_dir git cat-file blob $line[2]`;
	like($cur, qr/OMFG/, "kept original");
	$email->body_set("OMFG\n");
	$rm->remove_mime($email);
	@tree = `GIT_DIR=$git_dir git ls-tree -r HEAD`;
	is($?, 0, "no error from git ls-tree");
	is(scalar @tree, 0, "last removed");

	my @seq = qw(1 2 3);
	foreach my $i (@seq) {
		$email->body_set("$i\n");
		$mda->deliver($email);
	}
	@tree = `GIT_DIR=$git_dir git ls-tree -r HEAD`;
	is($?, 0, "no error from git ls-tree");
	is(scalar @tree, scalar @seq, "several messages exist");

	my $expect = 3;
	foreach my $i (@seq) {
		$email->body_set("$i\n");
		$rm->remove_mime($email);
		@tree = `GIT_DIR=$git_dir git ls-tree -r HEAD`;
		is($?, 0, "no error from git ls-tree");
		$expect--;
		is(scalar @tree, $expect, "$expect messages left");
	}

	{
		local $ENV{GIT_DIR} = $git_dir;
		my $before = `git rev-parse HEAD^0`;
		$rm->remove_mime($email);
		my $after = `git rev-parse HEAD^0`;
		is($before, $after, 'no commit on no-op removal');
	}

	{
		local $ENV{GIT_DIR} = $git_dir;
		$mda->deliver($email);
		my $before = `git rev-parse HEAD^0`;
		$email->body_set('changed');
		$rm->remove_mime($email);
		my $after = `git rev-parse HEAD^0`;
		is($before, $after, 'no commit on no-op removal miss');
	}
}

done_testing();
