#!/usr/bin/perl -w
# Copyright (C) 2013-2016 all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
use strict;
use warnings;
use Test::More;
use File::Temp qw/tempdir/;
use Email::MIME;
# test all command-line interfaces at once
my $mda = "blib/script/ssoma-mda";
my $cli = "blib/script/ssoma";
my $rm = "blib/script/ssoma-rm";
my $tmp = tempdir('ssoma-all-XXXXXX', TMPDIR => 1, CLEANUP => 1);
my $have_ipc_run = eval {
	require IPC::Run;
	import IPC::Run qw/run/;
	1;
};

ok(-x $mda, "$mda is executable");
ok(-x $cli, "$cli is executable");

{
	# instantiate new git repo
	my $git_dir = "$tmp/input.git";
	system(qw/git init -q --bare/, $git_dir) == 0 or
		die "git init -q --bare $git_dir failed: $?\n";
	ok(-d $git_dir && -f "$git_dir/config", '$GIT_DIR exists and is bare');

	# deliver the message
	my $mime = Email::MIME->new(<<'EOF');
From: me@example.com
To: u@example.com
Message-ID: <666@example.com>
Subject: zzz

OMFG
EOF
	my $pid = open my $pipe, '|-';
	defined $pid or die "failed to pipe + fork: $!\n";
	if ($pid == 0) {
		exec($mda, $git_dir);
		die "exec failed: $!\n";
	}
	print $pipe $mime->as_string or die "print failed: $!\n";
	close $pipe or die "close pipe failed: $!\n";
	is($?, 0, "$mda exited successfully");

	{
		local $ENV{GIT_DIR} = $git_dir;
		my @x = `git cat-file commit HEAD`;
		chomp @x;
		my @au = grep(/^author /, @x);
		like($au[0], qr/\Aauthor me <me\@example\.com>/, "author set");

		is('zzz', $x[-1], "subject set");
	}
}

{
	my $mbox = "$tmp/mbox";
	local $ENV{SSOMA_HOME} = "$tmp/ssoma-home";
	my $name = "test";
	my @cmd = ($cli, '-q', "add", $name, "$tmp/input.git", "mbox:$mbox");
	is(system(@cmd), 0, "add list with ssoma(1)");

	{
		use Ssoma::Git;
		my $git_dir = "$ENV{SSOMA_HOME}/$name.git";
		my $git = Ssoma::Git->new($git_dir);
		my $cfg = $git->config_list("$git_dir/ssoma.state");
		is(scalar keys %$cfg, 1, "only one key");
		like($cfg->{"target.local.path"}, qr{\A/},
		     "target.local.path is absolute");
		like($cfg->{"target.local.path"}, qr{\Q$mbox\E\z},
		     "target.local.path points to mbox");

		$cfg = $git->config_list("$git_dir/config");
		is($cfg->{"core.bare"}, "true", "repo is bare");
	}

	@cmd = ($cli, '-q', "sync");
	is(system(@cmd), 0, "sync list with ssoma(1)");

	open(my $fh, '<', $mbox) or die "open $mbox: $!\n";
	my @lines = <$fh>;
	is(scalar grep(/^Subject: zzz/, @lines), 1, "email delivered");
	close $fh or die "close $mbox: $!\n";
}

{
	# deliver an additional message
	my $mime = Email::MIME->new(<<'EOF');
From: moi@example.com
To: you@example.com
Message-ID: <666666@example.com>
Subject: xxx

OMFG
EOF
	my $pid = open my $pipe, '|-';
	defined $pid or die "failed to pipe + fork: $!\n";
	if ($pid == 0) {
		exec($mda, "$tmp/input.git");
		die "exec failed: $!\n";
	}
	print $pipe $mime->as_string or die "print failed: $!\n";
	close $pipe or die "close pipe failed: $!\n";
	is($?, 0, "$mda exited successfully");
}

# ensure new message is delivered
{
	my $mbox = "$tmp/mbox";
	local $ENV{SSOMA_HOME} = "$tmp/ssoma-home";
	my $name = "test";

	my @cmd = ($cli, '-q', "sync", $name);
	is(system(@cmd), 0, "sync $name list with ssoma(1)");

	open(my $fh, '<', $mbox) or die "open $mbox: $!\n";
	my @lines = <$fh>;
	is(scalar grep(/^Subject: xxx/, @lines), 1, "email delivered");
	is(scalar grep(/^Subject: zzz/, @lines), 1, "email delivered");
	close $fh or die "close $mbox: $!\n";
}

# ssoma cat functionality
{
	local $ENV{SSOMA_HOME} = "$tmp/ssoma-home";
	my @full = `$cli cat \\<666\@example.com\\>`;
	my $from = shift @full;
	like($from, qr/^From /, "ssoma cat mbox has From_ line");
	is(scalar grep(/^Message-ID: <666\@example\.com>/, @full), 1,
	   "correct message returned from ssoma cat");
	my @lazy = `$cli cat 666\@example.com`;
	$from = shift @lazy;
	like($from, qr/^From /, "ssoma cat (lazy) mbox has From_ line");
	is(join('', @lazy), join('', @full),
	    "lazy ssoma cat invocation w/o <> works");
}

# ssoma cat with a repo path
{
	my @full = `$cli cat \\<666\@example.com\\> $tmp/input.git`;
	my $from = shift @full;
	like($from, qr/^From /, "ssoma cat mbox has From_ line");
	is(scalar grep(/^Message-ID: <666\@example\.com>/, @full), 1,
	   "correct message returned from ssoma cat");
}

# duplicate message delivered to MDA (for "ssoma cat" dup handling)
{
	# deliver the message
	my $dup = Email::MIME->new(<<'EOF');
From: me@example.com
To: u@example.com
Message-ID: <666@example.com>
Subject: duplicate

EOF
	use Ssoma::MDA;
	use Ssoma::Git;
	Ssoma::MDA->new(Ssoma::Git->new("$tmp/input.git"))->deliver($dup);
}

# test ssoma cat on a duplicate
{
	my $mbox = "$tmp/mbox";
	local $ENV{SSOMA_HOME} = "$tmp/ssoma-home";
	my $name = "test";
	my @cmd = ($cli, "-q", "sync", $name);
	is(system(@cmd), 0, "sync $name with ssoma(1)");

	my @both = `$cli cat \\<666\@example.com\\>`;
	is(scalar grep(/^Message-ID: <666\@example\.com>/, @both), 2,
	   "correct messages returned from ssoma cat");
	is(scalar grep(/^From /, @both), 2,
	   "From_ line from both messages returned from ssoma cat");
	my @s = sort grep(/^Subject: /, @both);
	my @x = ("Subject: duplicate\n", "Subject: zzz\n");
	is_deeply(\@s, \@x, "subjects are correct in mbox");
}

# test ssoma-rm functionality
{
	my $git_dir = "$tmp/input.git";
	my @tree = `GIT_DIR=$git_dir git ls-tree -r HEAD`;
	is(scalar @tree, 3, "three messages sitting in a tree");

	# deliver the message to ssoma-rm
	my $mime = Email::MIME->new(<<'EOF');
From: me@example.com
To: u@example.com
Message-ID: <666@example.com>
Subject: zzz

OMFG
EOF
	my $pid = open my $pipe, '|-';
	defined $pid or die "failed to pipe + fork: $!\n";
	if ($pid == 0) {
		exec($rm, $git_dir);
		die "exec failed: $!\n";
	}
	print $pipe $mime->as_string or die "print failed: $!\n";
	close $pipe or die "close pipe failed: $!\n";
	is($?, 0, "$rm exited successfully");
	@tree = `GIT_DIR=$git_dir git ls-tree -r HEAD`;
	is(scalar @tree, 2, "two messages sitting in a tree");
}

# duplicate detection
SKIP: {
	skip "IPC::Run not available", 2 unless $have_ipc_run;
	my $mime = Email::MIME->new(<<'EOF');
From: moi@example.com
To: you@example.com
Message-ID: <666666@example.com>
Subject: xxx

OMFG
EOF
	$mime = $mime->as_string;
	my ($out, $err) = ("", "");
	run([$mda, "-1", "$tmp/input.git"], \$mime, \$out, \$err);
	isnt($?, 0, "$mda exited with failure");
	like($err, qr/CONFLICT/, "conflict message detected");
}

done_testing();
