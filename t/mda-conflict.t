#!/usr/bin/perl -w
# Copyright (C) 2013-2016 all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
use strict;
use warnings;
use Test::More;
use Ssoma::MDA;
use Ssoma::Git;
use Email::MIME;
use Digest::SHA qw/sha1_hex/;
use File::Temp qw/tempdir/;

my $tmpdir = tempdir('ssoma-mda-conflict-XXXXXX', TMPDIR => 1, CLEANUP => 1);
my $git = Ssoma::Git->new("$tmpdir/gittest");
$git->init_db('-q');
my $mda = Ssoma::MDA->new($git);

my $email = Email::MIME->new("From: U <u\@example.com>\n\nHIHI\n");
$email->header_set("To", "Me <me\@example.com>");
$email->header_set("Subject", ":o");
$email->header_set("Message-ID", "<12345\@example.com>");

$mda->deliver($email);

local $ENV{GIT_DIR} = "$tmpdir/gittest";
my @orig = `git rev-list HEAD`;
is(1, scalar @orig, "one revision exists");

# deliver a second message
$email->header_set("message-ID", "<666\@example.com>");
$email->body_set("BYEBYE\nBYEYBE\n");

$mda->deliver($email);

# validate delivery results and history
my @two = ` git rev-list HEAD`;
is(2, scalar @two, "two revisions exist");
is($orig[0], $two[1], "history is correct");

my @tree = `git ls-tree -r HEAD`;
is(0, $?, "git ls-tree -r HEAD succeeded");
chomp @tree;
is(2, scalar @tree, "two entries in tree");

# ensure path Message-ID -> path mapping works
foreach my $line (@tree) {
	my ($mode, $type, $blob, $path) = split(/\s+/, $line);;
	my $raw = `git cat-file blob $blob`;
	my $mime = Email::MIME->new($raw);
	my $mid = $mime->header("message-id");
	my $path_sha1 = $path;
	$path_sha1 =~ tr!/!!d;
	$mid =~ tr/<>//d;
	is($path_sha1, sha1_hex($mid), "path mapping works $mid");
}

# delivery again with identical Message-ID
$mda->deliver($email);

# duplicate detected
chomp(my @curr = `git ls-tree -r HEAD`);
is_deeply(\@tree, \@curr, "duplicate not stored");

# repeat message-ID but different content
$email->body_set("different\n");
$mda->deliver($email);

my @prev = @curr;
my @prev_blobs = map { (split(/\s+/, $_))[2] } @prev;

chomp(@curr = `git ls-tree -r HEAD`);
my %curr_blobs = map { (split(/\s+/, $_))[2] => 1 } @curr;
is(3, scalar @curr, "mismatch stored with identical Message-ID");

foreach my $prev (@prev_blobs) {
	ok(delete $curr_blobs{$prev}, "prev=$prev blob exists");
}

my @only = keys %curr_blobs;
is(1, scalar @only, "one new blob stored");

my $body_3 = "3rd message with identical Message-ID, ridiculous\n";
$email->body_set($body_3);
$mda->deliver($email);

@prev = @curr;
@prev_blobs = map { (split(/\s+/, $_))[2] } @prev;
chomp(@curr = `git ls-tree -r HEAD`);
%curr_blobs = map { (split(/\s+/, $_))[2] => 1 } @curr;
is(4, scalar @curr, "another stored with identical Message-ID");

foreach my $prev (@prev_blobs) {
	ok(delete $curr_blobs{$prev}, "prev=$prev blob exists");
}
@only = keys %curr_blobs;
is(1, scalar @only, "one new blob stored");

my $want = sha1_hex($email->header("Subject") . $email->body);
my @want = grep(m!/\Q$want\E!, @curr);
is(1, scalar @want, "wanted message is unique");
my $blob = (split(/\s+/, $want[0]))[2];
my $s = `git cat-file blob $blob`;
$s = Email::MIME->new($s);
is("<666\@example.com>", $s->header("message-id"), "MID matches");
is($body_3, $s->body, "body matches");

done_testing();
