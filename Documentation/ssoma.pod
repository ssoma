% ssoma(1) ssoma user manual

=head1 NAME

ssoma - mail archive synchronization and extraction client

=head1 SYNOPSIS

ssoma add LISTNAME URL maildir:/path/to/maildir/ [TARGET]

ssoma add LISTNAME URL mbox:/path/to/mbox

ssoma add LISTNAME URL imap://USER@HOST/INBOX

ssoma sync [--since=DATE] [LISTNAME]

ssoma cat MESSAGE-ID [LISTNAME|GIT_DIR]

=head1 DESCRIPTION

ssoma may be used to sync and export mail to Maildir, IMAP or L<mbox(5)>
from any published ssoma git repository.

=over

=item add LISTNAME URL DESTINATION [TARGET]

This starts a subscription to a mailing list by configuring a git
repository.  LISTNAME is a name of your choosing.  It must only consist
of alphanumeric characters, underscores, periods and dashes, and must start
and end with an alphanumeric character.  URL is the URL to a git repository,
this supports all URLs L<git(7)> supports.  DESTINATION is the local
destination to extract mail to.  This may be a maildir:, mbox: path,
or an imap:// or imaps:// URL.

The repository is stored in ~/.ssoma/$LISTNAME.git

If at any time a user wishes to stop following the list, just remove the
git repository from your file system.

IMAP users may configure the imap.pass and imap.tunnel variables in
~/.ssoma/$LISTNAME.git/config in the same way as L<git-imap-send(1)>.
Remember to restrict permissions to ~/.ssoma/$LISTNAME.git/config
if you are storing a password in it.

TARGET may optionally be specified if you wish to extract a list to multiple
destinations (e.g. an mbox for certain tools, but also to your IMAP account).
The default TARGET name is "local".

=item sync [--since=DATE] [LISTNAME] [TARGET]

This clones/fetches from the remote git repository into the local
repository and extracts messages into destinations configured with the
"add" sub-command.  If LISTNAME is not given, all list subscriptions are
synchronized.  If LISTNAME is given, only subscriptions for a given LISTNAME
is synchronized.  If TARGET is also given, the only the specified TARGET
is synchronized.

If you are following a list with a long history, you may only want to
extract recent messages by specifying --since=DATE and passing any DATE
format understood by L<git-log(1)>.

=item cat MESSAGE-ID [LISTNAME|GIT_DIR]

This outputs the message matching MESSAGE-ID to stdout (in mbox format).
If LISTNAME is given, this limits the Message-ID search to that list.

Specifying a GIT_DIR in place of LISTNAME is also possible, this is
intended for administrators using L<ssoma-rm(1)>.

=back

=head1 FILES

All client-side git repositories are stored in ~/.ssoma/$LISTNAME.git/

See L<ssoma_repository(5)> for details.

=head1 ENVIRONMENT VARIABLES

SSOMA_HOME may be used to override the default ~/.ssoma/ directory.
This is useful for testing, but not recommended otherwise.

=head1 CONTACT

The mail archives are hosted at L<git://public-inbox.org/meta>

See L<ssoma(1)> for instructions on how to subscribe.

You may subscribe using ssoma:

	ssoma add pi git://public-inbox.org/meta mbox:/path/to/mbox
	ssoma sync pi

=head1 SOURCE CODE

	git clone git://80x24.org/ssoma

=head1 COPYRIGHT

Copyright 2013-2016 all contributors L<mailto:meta@public-inbox.org>

License: AGPL-3.0+ L<http://www.gnu.org/licenses/agpl-3.0.txt>

=head1 SEE ALSO

L<git(1)>, L<ssoma_repository(5)>, L<ssoma-rm(1)>
