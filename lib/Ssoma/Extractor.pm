# Copyright (C) 2013-2016 all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
#
# Extracts mail to an Mbox or Maildir
package Ssoma::Extractor;
use strict;
use warnings;
use Ssoma::Git;
use Email::LocalDelivery;
use IO::File;

sub new {
	my ($class, $git) = @_;
	bless { git => $git, ref => "refs/heads/master" }, $class;
}

# runs a command which returns a list of files belonging to emails
# This won't prevent invalid/corrupt messages from attempts at
# being imported, though.  This allows administrators to add things
# like a top-level README file to avoid confusing folks who may
# accidentally check out the ssoma repository as a working copy.
sub _flist {
	my ($cmd) = @_;
	my @rv = `$cmd`;
	$? == 0 or die "$cmd failed: $?\n";
	chomp @rv;
	@rv = grep(m!\A[a-f0-9]{2}/[a-f0-9]{38}(?:/[a-f0-9]{40})?\z!, @rv);
	\@rv
}

sub warn_v2 {
	my ($self) = @_;
	if (system('git cat-file blob HEAD:m >/dev/null 2>&1') == 0 ||
	    system('git cat-file blob HEAD:d >/dev/null 2>&1') == 0) {
		warn <<"";
$self->{git}->{git_dir} looks like a public-inbox v2 repo.
ssoma is deprecated and does not support v2 repositories

	}
}

sub _extract {
	my ($self, $target, $since) = @_;
	my $git = $self->{git};

	# read all of the state file
	my $state = "$git->{git_dir}/ssoma.state";
	my $cfg = $git->config_list($state);

	my $pkey = "target.$target.path";
	my $path = $cfg->{$pkey};

	my $ckey = "target.$target.command";
	my $command = $cfg->{$ckey};

	my $ikey = "target.$target.imap";
	my $imap = $cfg->{$ikey};

	my $lkey = "target.$target.last-imported";
	my $last = $cfg->{$lkey};
	my $last_imported = $last;

	my $ref = $self->{ref};
	my $tip = $git->qx_sha1("git rev-parse $ref^0");

	my $new; # arrayref of new file pathnames in a git tree

	if (defined $since) {
		my @cmd = (qw(git rev-list), "--since=$since", $tip);
		my $tmp;

		# get the commit last in the list, unfortunately --reverse
		# is not usable with --since
		open my $rl, '-|', @cmd or die "failed to open rev-list: $!\n";
		foreach my $cmt (<$rl>) {
			chomp $cmt;

			# do not re-import even if --since is specified
			if (defined $last && ($last eq $cmt)) {
				$tmp = undef;
				last
			}
			$tmp = $cmt;
		}
		close $rl; # we may break the pipe here
		$last = $tmp if defined $tmp;
	}
	if (defined $last) {
		# only inject newly-added
		$last =~ /\A[a-f0-9]{40}\z/ or die "$lkey invalid in $state\n";

		# we don't want blob->tree conflict resolution in MDA
		# tricking us into extracting the same message twice;
		# MDA will keep the original in sufficiently-identical messages
		my $cmd = "git diff-tree -r --name-only -M100% --diff-filter=A";
		$new = [];
		if ($last ne $tip) {
			$new = _flist("$cmd $last $tip");
			warn_v2($self) unless scalar(@$new);
		}
	} else {
		# new maildir or mbox (to us), import everything in the
		# current tree
		$new = _flist("git ls-tree -r --name-only $tip");
		warn_v2($self) unless scalar(@$new);
	}

	my $i = 0;
	$i++ if defined $command;
	$i++ if defined $path;
	$i++ if defined $imap;
	($i > 1) and die
	       "only one of $pkey, $ckey, or $ikey may be defined in $state\n";

	if (defined $command) {
		$self->_run_for_each_msg($command, $tip, $new)
	} elsif (defined $path) {
		$self->_deliver_each_msg($path, $tip, $new);
	} elsif (defined $imap) {
		$self->_imap_deliver_each_msg($tip, $new);
	} else {
		die "neither $pkey, $ckey, nor $ikey are defined in $state\n";
	}

	# update the last-imported var
	if (!defined($last_imported) || $last_imported ne $tip) {
		# This state file is important, and git does not fsync it.
		if (open my $st, '<', $state) {
			# clobber existing backup
			my $bak = "$state.bak";
			open my $bk, '>', $bak or die
				"failed to open backup ($bak): $!";
			my $size = -s $st;
			my $r = read($st, my $buf, $size);
			$st->close or die "close $state failed for copy: $!";
			defined $r or die "failed read $size from $state: $!";
			$r == $size or die "read ($r != $size) from $state";
			$bk->write($buf) or die "failed to write $bak: $!";
			defined($bk->flush) or die "flush $bak failed: $!";
			defined($bk->sync) or die "fsync $bak failed: $!";
			$bk->close or die "close $bak failed: $!";
		}

		local $ENV{GIT_CONFIG} = $state;
		my $rv = system(qw/git config/, $lkey, $tip);
		$rv == 0 or die "git config $lkey $tip failed: $? ($rv)";

		# git-config(1) does not fsync state files by default
		open my $st, '+<', $state or
				die "failed to open state ($state): $!";
		defined($st->sync) or die "fsync $state failed: $!";
		$st->close or die "close $state failed for fsync: $!";
	}
}

# deliver to mbox or maildir, Email::LocalDelivery determines the type of
# folder (via Email::FolderType) via trailing trailing slash for maildir
# (and lack of trailing slash for mbox).  Ezmlm and MH formats are not
# currently supported by Email::LocalDelivery.
sub _deliver_each_msg {
	my ($self, $dest, $tip, $new) = @_;
	my $git = $self->{git};
	my $git_pm = $git->try_git_pm;
	foreach my $path (@$new) {
		_deliver_die($git->cat_blob("$tip:$path", $git_pm), $dest);
	}
}

# just pipe the blob message to $command, bypassing Perl,
# so there's no validation at all
sub _run_for_each_msg {
	my ($self, $command, $tip, $new) = @_;
	my $git = $self->{git};
	foreach my $path (@$new) {
		my $cmd = "git cat-file blob $tip:$path | $command";
		my $rv = system($cmd);
		$rv == 0 or die "delivery command: $cmd failed: $? ($rv)\n";
	}
}

sub _imap_deliver_each_msg {
	my ($self, $tip, $new) = @_;
	my $git = $self->{git};
	require Ssoma::IMAP;
	my $imap = Ssoma::IMAP->new($git);
	my $git_pm = $git->try_git_pm;
	foreach my $path (@$new) {
		$imap->imap_deliver($git->cat_blob("$tip:$path", $git_pm));
	}
	$imap->quit;
}

sub extract {
	my ($self, $target, $since) = @_;
	$self->{git}->tmp_git_do(sub { $self->_extract($target, $since) });
}

sub _deliver_die {
	my @rv = Email::LocalDelivery->deliver(@_);
	(scalar @rv == 1 && -f $rv[0]) or
		die "delivery to $_[1] failed: $!\n";
}

# implements "ssoma cat MESSAGE-ID"
sub midextract {
	my ($self, $message_id, $mbox) = @_;
	$self->{git}->tmp_git_do(sub {
		$self->_midextract($message_id, $mbox);
	});
}

sub _midextract {
	my ($self, $message_id, $mbox) = @_;
	my $git = $self->{git};
	my $path = $git->mid2path($message_id);
	my $ref = $self->{ref};
	my $tip = $git->qx_sha1("git rev-parse $ref^0");
	my $obj = "$tip:$path";
	my $type = $git->type($obj);
	if ($type eq "tree") { # unlikely
		$git->each_in_tree($obj, sub {
			my ($blob_id, $xpath) = ($1, $2);
			_deliver_die($git->cat_blob($blob_id), $mbox);
		});
	} elsif ($type eq "blob") {
		_deliver_die($git->cat_blob($obj), $mbox);
	} else {
		die "unhandled type: $type (obj=$obj)\n";
	}
}

1;
