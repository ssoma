# Copyright (C) 2013-2016 all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
#
# Mail Delivery Agent module, delivers mail into a ssoma git repo
package Ssoma::MDA;
use strict;
use warnings;
use Ssoma::GitIndexInfo;

sub new {
	my ($class, $git) = @_;
	bless { git => $git, ref => "refs/heads/master" }, $class;
}

# may convert existing blob to a tree
# returns false if message already exists
# returns true on successful delivery
sub blob_upgrade {
	my ($self, $gii, $new, $path) = @_;

	my $git = $self->{git};
	my $obj = "$self->{ref}^0:$path";
	my $cur = $git->blob_to_mime($obj);

	# do nothing if the messages match:
	return 0 if $git->mime_eq($cur, $new);

	# kill the old blob
	$gii->remove($path);

	# implicitly create a new tree via index with two messages
	foreach my $mime ($cur, $new) {
		my $id = $git->mime_to_blob($mime);
		my $path2 = $git->hash_mime2($mime);
		$gii->update("100644", $id, "$path/$path2");
	}
	1;
}

# used to update existing trees, which only happen when we have Message-ID
# conflicts
sub tree_update {
	my ($self, $gii, $new, $path) = @_;
	my $git = $self->{git};
	my $obj = "$self->{ref}^0:$path";
	my $cmd = "git ls-tree $obj";
	my @tree = `$cmd`;
	$? == 0 or die "$cmd failed: $!\n";
	chomp @tree;

	my $id = $git->mime_to_blob($new);
	my $path2 = $git->hash_mime2($new);

	# go through the existing tree and look for duplicates
	foreach my $line (@tree) {
		$line =~ m!\A100644 blob ([a-f0-9]{40})\t(([a-f0-9]{40}))\z! or
			die "corrupt repo: bad line from $cmd: $line\n";
		my ($xid, $xpath2) = ($1, $2);

		# do nothing if most of the message matches
		return 0 if $path2 eq $xpath2 || $id eq $xid;
	}

	# no duplicates found, add to the index
	$gii->update("100644", $id, "$path/$path2");
}

# this appends the given message-id to the git repo, requires locking
# (Ssoma::Git::sync_do)
sub append {
	my ($self, $path, $mime, $once) = @_;

	my $git = $self->{git};
	my $ref = $self->{ref};

	# $path is a path name we generated, so it's sanitized
	my $gii = Ssoma::GitIndexInfo->new;

	my $obj = "$ref^0:$path";
	my $cmd = "git cat-file -t $obj";
	my $type = `$cmd 2>/dev/null`;

	if ($? == 0) { # rare, object already exists
		chomp $type;
		if ($once) {
			my $mid = $mime->header_obj->header_raw("Message-ID");
			die "CONFLICT: Message-ID: $mid exists ($path)\n";
		}

		# we return undef here if the message already exists
		if ($type eq "blob") {
			# this may upgrade the existing blob to a tree
			$self->blob_upgrade($gii, $mime, $path) or return;
		} elsif ($type eq "tree") {
			# possibly add object to an existing tree
			$self->tree_update($gii, $mime, $path) or return;
		} else {
			# we're screwed if a commit/tag has the same SHA-1
			die "CONFLICT: `$cmd' returned: $type\n";
		}
	} else { # new message, just create a blob, common
		my $id = $git->mime_to_blob($mime);
		$gii->update('100644', $id, $path);
	}

	{
		my $from = $mime->header('From');
		my (@email) = ($from =~ /([^<\s]+\@[^>\s]+)/g);
		my $name = $from;
		$name =~ s/\s*\S+\@.*\S+\s*//;
		my $email = $email[0] || 'invalid@example';
		if ($name !~ /\S/ || $name =~ /[<>]/) {
			$name = $email[0];
			$name =~ s/\@.*//;
		}
		$name =~ s/\A\s*//;
		my $date = $mime->header('Date');
		my $subject = $mime->header("Subject");
		$subject = '(no subject)' unless defined $subject;
		local $ENV{GIT_AUTHOR_NAME} ||= $name;
		local $ENV{GIT_AUTHOR_EMAIL} ||= $email[0];
		local $ENV{GIT_AUTHOR_DATE} ||= $date if defined $date;
		$git->commit_index($gii, 0, $ref, $subject);
	}
}

# the main entry point takes an Email::MIME object
sub deliver {
	my ($self, $mime, $once) = @_;
	my $git = $self->{git};
	# convert the Message-ID into a path
	my $mid = $mime->header_obj->header_raw("Message-ID");

	# if there's no Message-ID, generate one to avoid too many conflicts
	# leading to trees
	if (!defined $mid || $mid =~ /\A\s*\z/) {
		$mid = '<' . $git->hash_mime2($mime) . '@localhost>';
		$mime->header_set("Message-ID", $mid);
	}
	my $path = $git->mid2path($mid);

	# kill potentially confusing/misleading headers
	foreach my $d (qw(bytes lines content-length)) {
		$mime->header_set($d);
	}

	my $sub = sub {
		$git->tmp_index_do(sub {
			$self->append($path, $mime, $once);
		});
	};
	$git->sync_do(sub { $git->tmp_git_do($sub) });
}

1;
