# Copyright (C) 2013-2016 all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
package Ssoma::Remover;
use strict;
use warnings;
use Ssoma::Git;
use Ssoma::GitIndexInfo;

sub new {
	my ($class, $git) = @_;
	bless { git => $git, ref => "refs/heads/master" }, $class;
}

sub remove_mime {
	my ($self, $mime) = @_;
	my $git = $self->{git};
	my $sub = sub {
		$git->tmp_index_do(sub {
			$self->_remove($mime);
		});
	};
	$git->sync_do(sub { $git->tmp_git_do($sub) });
}

# remove an Email::MIME object from the current index
sub _remove {
	my ($self, $mime) = @_;
	my $git = $self->{git};
	my $path = $git->mid2path($mime->header_obj->header_raw("Message-ID"));
	my $ref = $self->{ref};
	my $tip = $git->qx_sha1("git rev-parse $ref^0");
	my $obj = "$tip:$path";
	my $type = eval { $git->type($obj) };
	return unless defined $type;
	my (@keep, @remove);
	if ($type eq "tree") { # unlikely
		$git->each_in_tree($obj, sub {
			my ($blob_id, $xpath) = ($1, $2);
			my $tmp = $git->blob_to_mime($blob_id);
			if ($git->mime_eq($mime, $tmp)) {
				push @remove, "$path/$xpath";
			} else {
				push @keep, $blob_id;
			}
		});
	} elsif ($type eq "blob") { # likely
		my $tmp = $git->blob_to_mime($obj);
		if ($git->mime_eq($mime, $tmp)) {
			push @remove, $path;
		}
	} else {
		die "unhandled type=$type for obj=$obj\n";
	}
	return unless @remove;

	my $gii = Ssoma::GitIndexInfo->new;
	foreach my $rm (@remove) { $gii->remove($rm) }

	if (scalar(@keep) == 1) { # convert tree back to blob
		my $blob_id = $keep[0];
		$gii->remove($path);
		$gii->update('100644', $blob_id, $path);
	} elsif ((scalar(@keep) == 0) && ($type eq "tree")) {
		# this is not possible unless mime_eq changes over time
		$gii->remove($path);
	} # else: do nothing if (@keep > 1)

	# commit changes
	$git->commit_index($gii, 1, $ref, 'rm');
}

1;
