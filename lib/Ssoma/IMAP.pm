# Copyright (C) 2013-2016 all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
#
# IMAP delivery module, used by Ssoma::Extractor if Email::LocalDelivery
# is not available.  Since we are dependent on git, we use the same config
# settings as those used by git-imap-send(1)
package Ssoma::IMAP;
use strict;
use warnings;
use Ssoma::Git;
use Net::IMAP::Simple;

sub new {
	my ($class, $git) = @_;
	my $file = "$git->{git_dir}/config";
	my $cfg = $git->config_list($file);
	my %opts = ();
	my $self = bless { opts => \%opts }, $class;
	foreach my $k (qw/folder host user pass port tunnel/) {
		$self->{$k} = $cfg->{"imap.$k"};
	}

	check_unsupported($git, $cfg);

	my $imap;
	if ((my $host = $self->{host})) {
		$host =~ s!imap://!!;
		$host =~ s!imaps://!! and $opts{use_ssl} = 1;
		my $port = $self->{port};
		$host .= ":$port" if defined $port;
		$self->get_pass($host);
		$imap = Net::IMAP::Simple->new($host, %opts) or conn_fail();
		$imap->login($self->{user}, $self->{pass}) or
				die "Login failed: " . $imap->errstr . "\n";
	} elsif ((my $tunnel = $self->{tunnel})) {
		# XXX not tested
		$host = "cmd:$tunnel";
		$imap = Net::IMAP::Simple->new($host, %opts) or conn_fail();
	} else {
		die "neither imap.host nor imap.tunnel set in $file\n";
	}
	$self->{imap} = $imap;
	$self;
}

sub imap_deliver {
	my ($self, $msg) = @_;
	$self->{imap}->put($self->{folder}, $msg);
}

sub check_unsupported {
	my ($git, $cfg) = @_;

	if ((my $sslverify = $cfg->{"imap.sslverify"})) {
		local $ENV{GIT_CONFIG} = "$git->{git_dir}/config";
		$sslverify = `git config --bool imap.sslverify`;
		chomp $sslverify;
		if ($sslverify eq "false") {
			die "imap.sslverify=false not supported\n";
		}
	}

	if (defined $cfg->{"imap.authmethod"}) {
		die "imap.authMethod not supported by Net::IMAP::Simple\n";
	}
}

sub get_pass {
	my ($self, $host) = @_;

	return if defined $self->{pass};
	my $pass = "";

	print STDERR "$self->{user}\@$host password:";
	STDERR->flush;
	my $readkey;
	eval {
		require Term::ReadKey;
		Term::ReadKey::ReadMode('noecho');
	};
	if ($@) {
		my $cmd = 'stty -echo';
		print STDERR "Term::ReadKey not available, using `$cmd'\n";
		system($cmd) and die "$cmd failed: $?\n";
		$pass = <STDIN>;
		$cmd = 'stty echo';
		system($cmd) and die "$cmd failed: $?\n";
		chomp $pass;
	} else {
		# read the password
		while (defined(my $key = Term::ReadKey::ReadKey(0))) {
			last if $key =~ /[\012\015]/; # [\r\n]
			$pass .= $key;
		}
		Term::ReadKey::ReadMode('restore');
	}
	print STDERR "\n";
	STDERR->flush;

	$self->{pass} = $pass;
}

sub conn_fail {
      die "Unable to connect to IMAP: $Net::IMAP::Simple::errstr\n";
}

sub quit {
	my ($self) = @_;
	$self->{imap}->quit;
}

1;
